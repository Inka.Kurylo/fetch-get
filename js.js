
let body = document.querySelector('body');
class Card {
    constructor({ name, email, title, body, id }) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.container = document.createElement('div');
        this.deleteButton = document.createElement('button');
    }
    render() {
        this.container.insertAdjacentHTML('beforeend',
            `<div>
        
        <span>Name: ${this.name}</span><br>
        <span>Email: ${this.email}</span><br>
        <span>title: ${this.title}</span><br>
        <p>post ${this.id}: ${this.body}</p>
    </div>`);
        this.deleteButton.innerHTML = 'Delete';
        this.container.append(this.deleteButton);
        body.append(this.container);

        this.deleteButton.addEventListener('click', () => {
            let postId = this.id;
            fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                method: 'DELETE',
            })
                .then(({ status }) => {
                    console.log(status);
                    if (status === 200) {
                        this.container.remove();
                    }
                })
        })
    }
}
async function getUsersPosts() {
    try {
        let promiseUser = await fetch('https://ajax.test-danit.com/api/json/users')
            .then((response) => response.json())
        let promisePosts = await fetch('https://ajax.test-danit.com/api/json/posts')
            .then((res) => res.json())
        const userResponse = await Promise.allSettled([promisePosts, promiseUser]);
        return userResponse;
    } catch (e) {
        console.warn(e);
    }
}
async function createElement() {
    let arr = await getUsersPosts();
    let postList = arr[0].value;
    let userList = arr[1].value;
    let findUser = {};
    postList.forEach(post => {
        userList.forEach(el => {
            if (el.id === post.userId) {
                findUser.name = el.name;
                findUser.email = el.email;
                findUser.body = post.body;
                findUser.title = post.title;
                findUser.id = post.id;
            }
        })
        let card = new Card(findUser).render();
    });
}
createElement();
